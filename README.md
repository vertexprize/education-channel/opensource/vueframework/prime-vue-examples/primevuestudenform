# PrimeVueStudenForm

Пример приложенния для доступа к базе данных студентов

## Создание приложения

### Сформировать каркас приложения с использованием команды:

```sh
npm create vue@3
```

### Собрать приложение

Для сборки приложения необходимо перейти в каталог  `primevue-student-form`
и выполнить команду:

```sh
npm install
```

### Запустить каркас приложения

```sh
npm run dev
```

### Отредактировать приложение в среде разработки Visual Studio Code

В среде разработки выбрать для редактирования на файл    `main.js` 

```js-nolint
import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'

createApp(App).mount('#app')
```

 1. Убрать строку      `import './assets/main.css'`  из файла  `main.js` 
 1. Сохранить изменения в файле   `main.js` 
 1. Удалить из приложения стандартный  файл CSS `base.css`  так как он будет заменен файлом из фреймворка PrimeVue
 1. Удалить из файла `main.css` строку @import    `'./base.css'`;
 1. Удалит из приложения стандартный  файл  `logo.svg` с логотипом Vue
 1. Удалить из приложения  каталог `components` 
 1. Отредактировать файл  `AppVue`  для удаления ссылок на компоненты, из  каталога `components`

```js-nolint
<script setup>
import HelloWorld from './components/HelloWorld.vue'
import TheWelcome from './components/TheWelcome.vue'
</script>

<template>
  <header>
    <img alt="Vue logo" class="logo" src="./assets/logo.svg" width="125" height="125" />

    <div class="wrapper">
      <HelloWorld msg="You did it!" />
    </div>
  </header>

  <main>
    <TheWelcome />
  </main>
</template>

<style scoped>
header {
  line-height: 1.5;
}

 ...
```

Удалить:

- строку  `import HelloWorld from './components/HelloWorld.vue'` ;
- строку  `import TheWelcome from './components/TheWelcome.vue'` ;
- блок  `header` ;
- содержимое блока  `style scoped` .

Добавить в блок `<template>` строку `Привет мир`
Полученный исходный код:

```js-nolint
<script setup>
</script>

<template>
  <h1>Привет мир</h1>
</template>

<style scoped>
</style>
```

### Проверить работу приложения

После обновления браузера появится строка `Привет мир`.Исходный код сформированного приложения размещен в ветке `0.0.1`.

### Установить в приложение  `primevue-student-form` компоненты PrimeVue

Перейти на сайт  [PrimeVue] to search the web.

[PrimeVue]:  https://primevue.org/vite/

 1. Перейти в каталог приложения  `primevue-student-form`
 1. Установить компоненты с использованием команды:

```bash
npm add primevue
```

 1. Установить библиотеку иконнок

```bash
npm install primeicons
```

### Добавление сетевых компонент

```bash
npm install axios
```