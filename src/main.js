
import { createApp } from 'vue'
import App from './App.vue'



import PrimeVue from 'primevue/config';
import AutoComplete from 'primevue/autocomplete'
import Button from 'primevue/button'
import InputText from 'primevue/inputtext'
import Card from 'primevue/card'
import Toast from 'primevue/toast'
import ToastService from 'primevue/toastservice'
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import axios from 'axios';
import VueAxios from 'vue-axios';
import 'primevue/resources/themes/arya-green/theme.css';

const app = createApp(App);

app.use(ToastService)
app.use(VueAxios, axios)
app.use(PrimeVue, { ripple: true });
app.component('AutoComplete', AutoComplete);
app.component('Button', Button);
app.component('InputText', InputText);
app.component('Card', Card)
app.component('Toast', Toast)
app.component('DataTable', DataTable) 
app.component('Column', Column) 
app.provide('axios', app.config.globalProperties.axios) 

app.mount('#app')





